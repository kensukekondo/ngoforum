package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.example.demo.entity.ExtendedMessage;
import com.example.demo.entity.Message;
import com.example.demo.entity.Read;
import com.example.demo.repository.MessageRepository;
import com.example.demo.repository.ReadRepository;

@Service
@Transactional
public class MessageService {

	@Autowired
	MessageRepository messageRepository;

	@Autowired
	ReadRepository readRepository;

	// レコード制限取得
	public List<Message> findLimitMessage(int page) {
		return messageRepository.findLimit(page);
	}

	// レコード全件取得
	public List<Message> findAllMessage() {
		return messageRepository.findAll();
	}

	// レコード追加
	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	//レコード削除
	public void deleteMessage(Integer id) {
		messageRepository.deleteById(id);
	}

	//レコード1件取得
	public Message editMessage(Integer id) {
		Message message = (Message) messageRepository.findById(id).orElse(null);
		return message;
	}

	// 絞り込み
	public List<Message> searchMessageDateCategory(String start, String end, int page ,String category) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		//日付が入力されていない場合
		if (!StringUtils.hasText(start) || !StringUtils.hasText(end)) {
			return messageRepository.findAllByLikeCategory(page,category);
		}
		if (start != "") {
			start += " 00:00:00";
		} else {
			start = "2022-01-01 00:00:00";
		}

		if (end != "") {
			end += " 23:59:59";
		} else {
			Date now = new Date();
			end = df.format(now);
		}

		Date startDate = null;
		try {
			startDate = df.parse(start);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Date endDate = null;
		try {
			endDate = df.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//カテゴリーがにゅうりょくされていない場合、日付のみ検索
		if (!StringUtils.hasText(category)) {
			return messageRepository.findByLimitDateBetween(startDate, endDate ,page);
		}
		//日付とカテゴリーで検索
		return messageRepository.findByDateCategory(startDate, endDate, page ,category);
	}

	//結合データーの取得
	public List<ExtendedMessage> findDate() {
		return messageRepository.findMessageDate();
	}

	// Readレコード追加
	public void saveRead(Read readData) {
		readRepository.save(readData);
	}

	// Readレコード全件取得
	public List<Read> findRead() {
		return readRepository.findAll();
	}

	//Readレコード1件取得
	public List<Read> findReadByUserId(Integer readUserId) {
		return readRepository.findReadByUserId(readUserId);
	}

}