package com.example.demo.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.User;
import com.example.demo.repository.LoginRepository;

@Service
@Transactional
public class LoginService {
	@Autowired
	LoginRepository loginRepository;

	//アカウント情報を取得
	public List<User> selectAccount(String account, String password) {
		return loginRepository. findByAccount(account, password);
	}

	//レコード1件取得
	public User editUser(Integer id) {
		User user = (User) loginRepository.findById(id).orElse(null);
		return user;
	}
}
