package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.User;
import com.example.demo.repository.SignUpRepository;

@Service
@Transactional
public class SignUpService {

	@Autowired
	SignUpRepository signupRepository;

	// アカウント登録
	public void saveUser(User user) {
		signupRepository.save(user);
	}

	//アカウント情報を取得
	public List<User> selectAccount(String account) {
		return signupRepository.findByAccount(account);
	}

}
