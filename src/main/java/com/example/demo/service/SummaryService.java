package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.repository.BranchRepository;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.ManageUserRepository;
import com.example.demo.repository.MessageRepository;

@Service
@Transactional
public class SummaryService {

	@Autowired
	ManageUserRepository manageUserRepository;

	@Autowired
	MessageRepository messageRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	CommentRepository commentRepository;

	// ユーザー全件取得
	public List<User> findAllUser() {
		return manageUserRepository.findAll();
	}

	// メッセージ全件取得
	public List<Message> findAllMessage() {
		return messageRepository.findAll();
	}

	// コメント全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAll();
	}

	// 支社全件取得
	public List<Branch> findAllBranche() {
		return branchRepository.findAll();
	}
}