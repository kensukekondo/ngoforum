package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
@Transactional
public class UserService {

	@Autowired
	UserRepository userRepository;

	// レコード全件取得
	public List<User> findAllUser() {
		return userRepository.findAll();
	}

	// レコード追加
	public void saveUser(User user) {
		userRepository.save(user);
	}

	//レコード削除
	public void deleteUser(Integer id) {
		userRepository.deleteById(id);
	}

	//レコード1件取得
	public User editUser(Integer id) {
		User user = (User) userRepository.findById(id).orElse(null);
		return user;
	}

}
