package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Comment;
import com.example.demo.entity.ExtendedComment;
import com.example.demo.repository.CommentRepository;

@Service
@Transactional
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

	// レコード全件取得
	public List<Comment> findAllComment() {
		return commentRepository.findAll();
	}

	// レコード追加
	public void saveComment(Comment comment) {
		comment.setCreated_date(Timestamp.valueOf(getTime()));
		comment.setUpdated_date(Timestamp.valueOf(getTime()));
		commentRepository.save(comment);
	}

	//レコード削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

	//レコード1件取得
	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	// 日付フォーマット
	private String getTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	//結合データーの取得
		public List<ExtendedComment> findDate() {
			return commentRepository.findCommentDate();
		}
}
