package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.User;
import com.example.demo.repository.ManageUserRepository;

@Service
@Transactional

public class ManageUserService {

	@Autowired
	ManageUserRepository manageuserRepository;

	// ユーザー全件取得
	public List<User> findAllUser() {
		return manageuserRepository.findAll();
	}

	// アカウント復活停止処理
	public void changeStatusUser(User user) {
		manageuserRepository.save(user);
	}

}
