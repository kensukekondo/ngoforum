package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.entity.PageMessage;
import com.example.demo.repository.PageRepository;

@Service
public class PageService {

    @Autowired
    private PageRepository pageRepository;

    public Page<PageMessage> getAllWord(Pageable pageable) {

        return pageRepository.findAll(pageable);
    }

    // 絞り込み
    public Page<PageMessage> searchMessageDateCategory(Pageable pageable, String start, String end, String category) {
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    	//日付が入力されていない場合
    	if (!StringUtils.hasText(start) || !StringUtils.hasText(end)) {
    		return pageRepository.findAllByLikeCategory(pageable, category);
    	}
    	if (start != "") {
    		start += " 00:00:00";
    	} else {
    		start = "2022-01-01 00:00:00";
    	}

    	if (end != "") {
    		end += " 23:59:59";
    	} else {
    		Date now = new Date();
    		end = df.format(now);
    	}

    	Date startDate = null;
    	try {
    		startDate = df.parse(start);
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}

    	Date endDate = null;
    	try {
    		endDate = df.parse(end);
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}
    	//カテゴリーがにゅうりょくされていない場合、日付のみ検索
    	if (!StringUtils.hasText(category)) {
    		return pageRepository.findByLimitDateBetween(pageable, startDate, endDate);
    	}
    	//日付とカテゴリーで検索
    	return pageRepository.findByDateCategory(pageable, startDate, endDate, category);
    }

}