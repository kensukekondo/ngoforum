package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.User;
import com.example.demo.repository.EditUserRepository;

@Service
@Transactional

public class EditUserService {

	@Autowired
	EditUserRepository edituserRepository;

	// アカウント情報取得
	public User editUser(Integer id) {
		User user = (User) edituserRepository.findById(id).orElse(null);
		return user;
	}

	// アカウント編集
	public void updateUser(User user) {
		edituserRepository.save(user);
	}

	//アカウント情報を取得
	public List<User> selectAccount(String account) {
		return edituserRepository.findByAccount(account);
	}

	//アカウント情報を取得
		public List<User> selectUser(int userId) {
			return edituserRepository.findById(userId);
		}

}
