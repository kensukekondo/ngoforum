package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "messages")
public class Message {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private int id; // 投稿ID
	@Column
	private String title; // 投稿タイトル
	@Column
	private String text; // 投稿本文
	@Column
	private String category; // 投稿カテゴリー
	@Column
	private Integer user_id; // 投稿者ID
	@Column
	private Date created_date; // 登録日時
	@Column
	private Date updated_date; // 更新日時

	@Transient
	private String defference_date; //投稿日時と現在日時の差を格納

	@Transient
	private Integer branch_id;

	@Transient
	private Integer department_id;

	@Transient
	private Integer already_read; //既読情報


	// 投稿IDを取得
	public int getId() {
		return id;
	}

	// 投稿IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 投稿タイトルを取得
	public String getTitle() {
		return title;
	}

	// 投稿タイトルを格納
	public void setTitle(String title) {
		this.title = title;
	}

	// 投稿本文を取得
	public String getText() {
		return text;
	}

	// 投稿を格納
	public void setText(String text) {
		this.text = text;
	}

	// 投稿カテゴリーを取得
	public String getCategory() {
		return category;
	}

	// 投稿カテゴリーを格納
	public void setCategory(String category) {
		this.category = category;
	}

	// 投稿者IDを取得
	public Integer getUser_id() {
		return user_id;
	}

	// 投稿者IDを格納
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	// 登録日時を取得
	public Date getCreated_date() {
		return created_date;
	}

	// 登録日時を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// 更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}

	// 更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	//投稿日時と現在日時の差を格納
	public String getDefference_date() {
		return defference_date;
	}

	//投稿日時と現在日時の差を格納
	public void setDefference_date(String defference_date) {
		this.defference_date = defference_date;
	}


	public Integer getBranch_id() {
		return branch_id;
	}


	public void setBranch_id(Integer branch_id) {
		this.branch_id = branch_id ;
	}


	public Integer getDepartment_id() {
		return department_id;
	}


	public void setDepartment_id(Integer department_id) {
		this.department_id = department_id ;
	}

	//既読情報を格納
	public Integer getAlready_read() {
		return already_read;
	}

	//既読情報を格納
	public void setAlready_read(Integer already_read) {
		this.already_read = already_read;
	}


}