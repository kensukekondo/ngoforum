package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "branches")
public class Branch {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // 支社ID
	@Column
	private String name; // 支社名
	@Column
	private Date created_date; // 登録日時
	@Column
	private Date updated_date; // 更新日時
	@Transient
	private int branchMessageCount; //支社毎のメッセージ数
	@Transient
	private int branchCommentCount; //支社毎のコメント数


	// 部署IDを取得
	public int getId() {
		return id;
	}
	// 部署IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 部署名を取得
	public String getName() {
		return name;
	}
	// 部署名を格納
	public void setName(String name) {
		this.name = name;
	}

	// 登録日時を取得
	public Date getCreated_date() {
		return created_date;
	}
	// 登録日時を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// 更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}
	// 更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	// 支社ごとのメッセージ数を取得
	public int getBranchMessageCount() {
		return branchMessageCount;
	}
	// 支社ごとのメッセージ数を格納
	public void setBranchMessageCount(int branchMessageCount) {
		this.branchMessageCount = branchMessageCount;
	}

	// 支社ごとのコメント数を取得
	public int getBranchCommentCount() {
		return branchCommentCount;
	}
	// 支社ごとのコメント数を格納
	public void setBranchCommentCount(int branchCommentCount) {
		this.branchCommentCount = branchCommentCount;
	}
}