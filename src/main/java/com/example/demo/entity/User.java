package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "users")
public class User {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // ユーザーID
	@Column
	private String account; // アカウント
	@Column
	private String password; // パスワード
	@Column
	private String name; // 氏名
	@Column
	private Integer branch_id; // 支社ID
	@Column
	private Integer department_id; // 部署ID
	@Column
	private Integer is_stopped; // ユーザー停止状態
	@Column
	private Date created_date; // 登録日時
	@Column
	private Date updated_date; // 更新日時
	@Column
	private Date login_date; // ログイン日時
	@Transient
	private int messageCount; //ユーザー毎のメッセージ数
	@Transient
	private int commentCount; //ユーザー毎のコメント数
	@Transient
	private int branchMessageCount; //支社毎のメッセージ数
	@Transient
	private int branchCommentCount; //支社毎のコメント数


	// 投稿IDを取得
	public int getId() {
		return id;
	}
	// 投稿IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// アカウントを取得
	public String getAccount() {
		return account;
	}
	// アカウントを格納
	public void setAccount(String account) {
		this.account = account;
	}

	// パスワードを取得
	public String getPassword() {
		return password;
	}
	// パスワードを格納
	public void setPassword(String password) {
		this.password = password;
	}

	// 氏名を取得
	public String getName() {
		return name;
	}
	// 氏名を格納
	public void setName(String name) {
		this.name = name;
	}

	// 支社IDを取得
	public Integer getBranch_id() {
		return branch_id;
	}
	// 支社IDを格納
	public void setBranch_id(Integer branch_id) {
		this.branch_id = branch_id;
	}

	// 部署IDを取得
	public Integer getDepartment_id() {
		return department_id;
	}
	// 部署IDを格納
	public void setDepartment_id(Integer department_id) {
		this.department_id = department_id;
	}


	// ユーザー停止状態を取得
	public int getIs_stopped() {
		return is_stopped;
	}
	// ユーザー停止状態を格納
	public void setIs_stopped(int Is_stopped) {
		this.is_stopped = Is_stopped;
	}


	// 登録日時を取得
	public Date getCreated_date() {
		return created_date;
	}
	// 登録日時を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// 更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}
	// 更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	// 更新時間を取得
	public Date getLogin_date() {
		return login_date;
	}
	// 更新時間を格納
	public void setLogin_date(Date login_date) {
		this.login_date = login_date;
	}

	// ユーザーごとのメッセージ数を取得
	public int getMessageCount() {
		return messageCount;
	}
	// ユーザーごとのメッセージ数を格納
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	// ユーザーごとのコメント数を取得
	public int getCommentCount() {
		return commentCount;
	}
	// ユーザーごとのコメント数を格納
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	// 支社ごとのメッセージ数を取得
	public int getBranchMessageCount() {
		return messageCount;
	}
	// 支社ごとのメッセージ数を格納
	public void setBranchCount(int branchMessageCount) {
		this.branchMessageCount = branchMessageCount;
	}

	// 支社ごとのコメント数を取得
	public int getBranchCommentCount() {
		return branchCommentCount;
	}
	// 支社ごとのコメント数を格納
	public void setBranchCommentCount(int branchCommentCount) {
		this.branchCommentCount = branchCommentCount;
	}
}