package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
public class Comment {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // 返信ID
	@Column
	private String text; // 返信本文
	@Column
	private Integer user_id; // 投稿者ID
	@Column
	private Integer message_id; // 投稿ID
	@Column
	private Date created_date; // 登録日時
	@Column
	private Date updated_date; // 更新日時

	// 返信IDを取得
	public int getId() {
		return id;
	}
	// 返信IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 投稿本文を取得
	public String getText() {
		return text;
	}
	// 投稿本文を格納
	public void setText(String text) {
		this.text = text;
	}

	// 投稿者IDを取得
	public Integer getUser_id() {
		return user_id;
	}
	// 投稿者IDを格納
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	// 投稿IDを取得
	public Integer getMessage_id() {
		return message_id;
	}
	// 投稿IDを格納
	public void setMessage_id(Integer message_id) {
		this.message_id = message_id;
	}

	// 登録日時を取得
	public Date getCreated_date() {
		return created_date;
	}
	// 登録日時を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// 更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}
	// 更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}