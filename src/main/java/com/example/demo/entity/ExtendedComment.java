package com.example.demo.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExtendedComment {
	private String account;
    private String name;
    private Integer branch_id;
    private Integer department_id;
    private Integer user_id;
    //以下追加
	private String text;
	private Integer message_id;
	private Date created_date; // 登録日時
	private Date updated_date; // 更新日時

	public ExtendedComment(Object[] objects) {
        this((String) objects[0], (String) objects[1], (Integer) objects[2],(Integer) objects[3],(Integer) objects[4],
        		(String) objects[5],(Integer) objects[6],(Date) objects[7],(Date) objects[8]);
    }

}
