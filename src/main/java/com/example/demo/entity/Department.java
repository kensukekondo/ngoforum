package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "departments")
public class Department {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; // 部署ID
	@Column
	private String name; // 部署名
	@Column
	private Date created_date; // 登録日時
	@Column
	private Date updated_date; // 更新日時

	// 部署IDを取得
	public int getId() {
		return id;
	}
	// 部署IDを格納
	public void setId(int id) {
		this.id = id;
	}

	// 部署名を取得
	public String getName() {
		return name;
	}
	// 部署名を格納
	public void setName(String name) {
		this.name = name;
	}

	// 登録日時を取得
	public Date getCreated_date() {
		return created_date;
	}
	// 登録日時を格納
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	// 更新時間を取得
	public Date getUpdated_date() {
		return updated_date;
	}
	// 更新時間を格納
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
}