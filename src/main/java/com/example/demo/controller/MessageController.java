package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;


@Controller
public class MessageController {

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	  HttpSession session;

	// 新規投稿画面
	@GetMapping("/NewMessage/{id}")
	public ModelAndView newContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Message message = new Message(); // form用の空のentityを準備
		message.setUser_id(id);
		mav.setViewName("/NewMessage"); // 画面遷移先を指定
		mav.addObject("formModel", message); // 準備した空のentityを保管
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		return mav;
	}

	// 投稿処理
	@PostMapping("/NewMessage/{id}")
	public ModelAndView addContent(@PathVariable Integer id ,@ModelAttribute("formModel") Message message) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		//件名が空白の場合
		if (!StringUtils.hasText(message.getTitle())) {
			errorMessages.add("件名を入力してください");
		}
		//件名が30文字より多い場合
		if (message.getTitle().length() > 30) {
			errorMessages.add("件名は30文字以下で入力してください");
		}
		//本文が空白の場合
		if (!StringUtils.hasText(message.getText())) {
			errorMessages.add("本文を入力してください");
		}
		//本文が1000文字より多い場合
		if (message.getText().length() > 1000) {
			errorMessages.add("本文は1000文字以下で入力してください");
		}
		//カテゴリーが空白の場合
		if (!StringUtils.hasText(message.getCategory())) {
			errorMessages.add("カテゴリーを入力してください");
		}
		//カテゴリーが10文字より多い場合
		if (message.getCategory().length() > 10) {
			errorMessages.add("カテゴリーは10文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			Message newMessage = new Message(); // form用の空のentityを準備
			newMessage.setTitle(message.getTitle());
			newMessage.setText(message.getText());
			newMessage.setCategory(message.getCategory());
			newMessage.setUser_id(id);
			mav.setViewName("/NewMessage"); // 画面遷移先を指定
			mav.addObject("formModel", newMessage); // 値を保持
			mav.addObject("errorMessages" , errorMessages);
			mav.addObject("loginUser", session.getAttribute("loginUser"));
			return mav;
		}

		String title = message.getTitle();
		String text = message.getText();
		String category = message.getCategory();

		// NGワードの設定
		String str = "test";
		String[] ngWord = str.split(",");

		for (int i = 0; i < ngWord.length; i++) {
			String s = ngWord[i];
			title = title.replaceAll(s, "****");
			text = text.replaceAll(s, "****");
			category = category.replaceAll(s, "****");
		}

		message.setId(0);
		message.setTitle(title);
		message.setText(text);
		message.setCategory(category);
		message.setUser_id(id);
		Date date = new Date();
		message.setCreated_date(date);
		message.setUpdated_date(date);
		messageService.saveMessage(message); // 投稿をテーブルに格納
		return new ModelAndView("redirect:/ngoforum"); // rootへリダイレクト
	}

	// 投稿削除処理
		@GetMapping("/delete/{id}")
		public ModelAndView deleteContent(@PathVariable Integer id) {
			// 返信を全件取得
			List<Comment> commentData = commentService.findAllComment();

			// 投稿に紐付くコメントの削除
			for (int i = 0; i < commentData.size(); i++) {
				Integer message_id = commentData.get(i).getMessage_id();
				if(message_id == id) {
					Integer comment_id = commentData.get(i).getId();
					commentService.deleteComment(comment_id);
				}
			}

			messageService.deleteMessage(id); // UrlParameterのidを基に投稿を削除
			return new ModelAndView("redirect:/ngoforum"); // rootへリダイレクト
		}

}
