package com.example.demo.controller;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.ExtendedMessage;
import com.example.demo.entity.Message;
import com.example.demo.entity.PageMessage;
import com.example.demo.entity.Read;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.PageService;
import com.example.demo.service.UserService;

@Controller
public class CommentController {
	@Autowired
	CommentService CommentService;

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	UserService userService;

	@Autowired
	PageService pageService;

	@Autowired
	HttpSession session;

	// 返信処理
	@PostMapping("/addComment")
	public ModelAndView addComment(@RequestParam(defaultValue = "0") int page,@ModelAttribute("formModel") Comment comment ,Model model, Pageable pageable) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		page = page*10;

		//本文が空白の場合
		if (!StringUtils.hasText(comment.getText())) {
			errorMessages.add("コメントを入力してください");
			mav.addObject("errorMessages", errorMessages);

			// 投稿を絞り込み取得
			List<Message> contentData = messageService.findLimitMessage(page);
			//結合データー取得
			List<ExtendedMessage> messageData = messageService.findDate();
			for(int i = 0; i < contentData.size(); i ++) {
				contentData.get(i).setBranch_id(messageData.get(i).getBranch_id());
				contentData.get(i).setDepartment_id(messageData.get(i).getDepartment_id());
			}
			//投稿日時の表示設定
			for (int i = 0; i < contentData.size(); i++) {
				contentData.get(i).getCreated_date();
				//投稿日時取得
				Calendar createdDate = Calendar.getInstance();
				createdDate.setTime(contentData.get(i).getCreated_date());
				//現在日時取得
				Date date = new Date();
				Calendar currentTime = Calendar.getInstance();
				currentTime.setTime(date);

				//投稿日時をLocalDateTimeへ変換
				LocalDateTime localCurrentTime = LocalDateTime.of(currentTime.get(Calendar.YEAR),
						currentTime.get(Calendar.MONTH),
						currentTime.get(Calendar.DAY_OF_MONTH), currentTime.get(Calendar.HOUR_OF_DAY),
						currentTime.get(Calendar.MINUTE), currentTime.get(Calendar.SECOND));
				//現在日時をLocalDateTimeへ変換
				LocalDateTime localCreatedDate = LocalDateTime.of(createdDate.get(Calendar.YEAR),
						createdDate.get(Calendar.MONTH),
						createdDate.get(Calendar.DAY_OF_MONTH), createdDate.get(Calendar.HOUR_OF_DAY),
						createdDate.get(Calendar.MINUTE), createdDate.get(Calendar.SECOND));
				//日時比較
				Duration defferenceDate = Duration.between(localCreatedDate, localCurrentTime);
				//日の差が1以上なら
				if (defferenceDate.toDays() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toDays() + "日前");
					//時間の差が1以上なら
				} else if (defferenceDate.toHours() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toHours() + "時間前");
					//分の差が1以上なら
				} else if (defferenceDate.toMinutes() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toMinutes() + "分前");
				} else {
					//秒の差をセット
					contentData.get(i).setDefference_date(defferenceDate.getSeconds() + "秒前");
				}
			}
			// 返信を全件取得
			List<Comment> commentData = commentService.findAllComment();
			// ユーザーを全件取得
			List<User> userData = userService.findAllUser();
			//既読情報を取得
			@SuppressWarnings("unchecked")
			List<User> user = (List<User>) session.getAttribute("loginUser");
			Integer readUserId = user.get(0).getId();
			List<Read> readData = messageService.findReadByUserId(readUserId);
			for (int i = 0; i < contentData.size(); i++) {
				for (int j = 0; j < readData.size(); j++) {
					if (contentData.get(i).getId() == readData.get(j).getMessage_id()) {
						contentData.get(i).setAlready_read(1);
					}
				}
			}
			// 画面遷移先を指定
			mav.setViewName("/top");
			// 投稿データオブジェクトを保管
			mav.addObject("contents", contentData);
			// 返信データオブジェクトを保管
			mav.addObject("comments", commentData);
			// ユーザーデータオブジェクトを保管
			mav.addObject("user", userData);
			//結合データオブジェクトを保管
			mav.addObject("messageData", messageData);
			mav.addObject("loginUser", session.getAttribute("loginUser"));

			Page<PageMessage> pageMessages = pageService.getAllWord(pageable);
			model.addAttribute("page", pageMessages);
			model.addAttribute("words", pageMessages.getContent());
			model.addAttribute("url", "/ngoforum");
			return mav;
		}
		//本文が500文字より多い場合
		if (comment.getText().length() > 500) {
			errorMessages.add("本文は500文字以下で入力してください");
			mav.addObject("errorMessages", errorMessages);

			// 投稿を絞り込み取得
			List<Message> contentData = messageService.findLimitMessage(page);
			//結合データー取得
			List<ExtendedMessage> messageData = messageService.findDate();
			for(int i = 0; i < contentData.size(); i ++) {
				contentData.get(i).setBranch_id(messageData.get(i).getBranch_id());
				contentData.get(i).setDepartment_id(messageData.get(i).getDepartment_id());
			}
			//投稿日時の表示設定
			for (int i = 0; i < contentData.size(); i++) {
				contentData.get(i).getCreated_date();
				//投稿日時取得
				Calendar createdDate = Calendar.getInstance();
				createdDate.setTime(contentData.get(i).getCreated_date());
				//現在日時取得
				Date date = new Date();
				Calendar currentTime = Calendar.getInstance();
				currentTime.setTime(date);

				//投稿日時をLocalDateTimeへ変換
				LocalDateTime localCurrentTime = LocalDateTime.of(currentTime.get(Calendar.YEAR),
						currentTime.get(Calendar.MONTH),
						currentTime.get(Calendar.DAY_OF_MONTH), currentTime.get(Calendar.HOUR_OF_DAY),
						currentTime.get(Calendar.MINUTE), currentTime.get(Calendar.SECOND));
				//現在日時をLocalDateTimeへ変換
				LocalDateTime localCreatedDate = LocalDateTime.of(createdDate.get(Calendar.YEAR),
						createdDate.get(Calendar.MONTH),
						createdDate.get(Calendar.DAY_OF_MONTH), createdDate.get(Calendar.HOUR_OF_DAY),
						createdDate.get(Calendar.MINUTE), createdDate.get(Calendar.SECOND));
				//日時比較
				Duration defferenceDate = Duration.between(localCreatedDate, localCurrentTime);
				//日の差が1以上なら
				if (defferenceDate.toDays() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toDays() + "日前");
					//時間の差が1以上なら
				} else if (defferenceDate.toHours() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toHours() + "時間前");
					//分の差が1以上なら
				} else if (defferenceDate.toMinutes() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toMinutes() + "分前");
				} else {
					//秒の差をセット
					contentData.get(i).setDefference_date(defferenceDate.getSeconds() + "秒前");
				}
			}
			// 返信を全件取得
			List<Comment> commentData = commentService.findAllComment();
			// ユーザーを全件取得
			List<User> userData = userService.findAllUser();
			//既読情報を取得
			@SuppressWarnings("unchecked")
			List<User> user = (List<User>) session.getAttribute("loginUser");
			Integer readUserId = user.get(0).getId();
			List<Read> readData = messageService.findReadByUserId(readUserId);
			for (int i = 0; i < contentData.size(); i++) {
				for (int j = 0; j < readData.size(); j++) {
					if (contentData.get(i).getId() == readData.get(j).getMessage_id()) {
						contentData.get(i).setAlready_read(1);
					}
				}
			}
			// 画面遷移先を指定
			mav.setViewName("/top");
			// 投稿データオブジェクトを保管
			mav.addObject("contents", contentData);
			// 返信データオブジェクトを保管
			mav.addObject("comments", commentData);
			// ユーザーデータオブジェクトを保管
			mav.addObject("user", userData);
			//結合データオブジェクトを保管
			mav.addObject("messageData", messageData);
			mav.addObject("loginUser", session.getAttribute("loginUser"));

			Page<PageMessage> pageMessages = pageService.getAllWord(pageable);
			model.addAttribute("page", pageMessages);
			model.addAttribute("words", pageMessages.getContent());
			model.addAttribute("url", "/ngoforum");
			return mav;
		}

		CommentService.saveComment((com.example.demo.entity.Comment) comment); // 返信をテーブルに格納
		return new ModelAndView("redirect:/ngoforum"); // rootへリダイレクト
	}

	// 返信削除処理
	@GetMapping("/deleteComment/{id}")
	public ModelAndView deleteCommentContent(@PathVariable Integer id) {
		CommentService.deleteComment(id); // UrlParameterのidを基に投稿を削除
		return new ModelAndView("redirect:/ngoforum"); // rootへリダイレクト
	}
}
