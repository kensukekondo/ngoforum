package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Logout {
	@Autowired
	HttpSession session;

	@GetMapping("/Logout")
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response){
	ModelAndView mav = new ModelAndView();
     // セッションの無効化
     session.invalidate();
     mav.setViewName("/login");
     return mav;
	}
}
