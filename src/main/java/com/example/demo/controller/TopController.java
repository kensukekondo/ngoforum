package com.example.demo.controller;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.ExtendedMessage;
import com.example.demo.entity.Message;
import com.example.demo.entity.PageMessage;
import com.example.demo.entity.Read;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.PageService;
import com.example.demo.service.UserService;

@Controller
public class TopController {

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	UserService userService;

	@Autowired
	PageService pageService;

	@Autowired
	HttpSession session;;

	// 投稿内容表示画面
	@GetMapping("/ngoforum")
	public ModelAndView top(@RequestParam(defaultValue = "0") int page,Model model, Pageable pageable,
			@RequestParam(defaultValue = "", name = "remove") String remove) {
		ModelAndView mav = new ModelAndView();

		if (remove == "remove") {
			session.removeAttribute("start");
			session.removeAttribute("end");
			session.removeAttribute("category");
		}

		String start =(String) session.getAttribute("start");
		String end =(String) session.getAttribute("end");
		String category =(String) session.getAttribute("category");
		page = page*10;
		List<Message> contentData = new ArrayList();
		if (start != null || end != null || category != null) {
			// 絞り込み後の投稿の取得
			contentData = messageService.searchMessageDateCategory(start, end, page ,category);
		} else {
			// 投稿を絞り込み取得
			contentData = messageService.findLimitMessage(page);
		}

		//結合データー取得
		List<ExtendedMessage> messageData = messageService.findDate();
		for(int i = 0; i < contentData.size(); i ++) {
			contentData.get(i).setBranch_id(messageData.get(i).getBranch_id());
			contentData.get(i).setDepartment_id(messageData.get(i).getDepartment_id());
		}
		//投稿日時の表示設定
		for (int i = 0; i < contentData.size(); i++) {
			contentData.get(i).getCreated_date();
			//投稿日時取得
			Calendar createdDate = Calendar.getInstance();
			createdDate.setTime(contentData.get(i).getCreated_date());
			//現在日時取得
			Date date = new Date();
			Calendar currentTime = Calendar.getInstance();
			currentTime.setTime(date);

			//投稿日時をLocalDateTimeへ変換
			LocalDateTime localCurrentTime = LocalDateTime.of(currentTime.get(Calendar.YEAR),
					currentTime.get(Calendar.MONTH),
					currentTime.get(Calendar.DAY_OF_MONTH), currentTime.get(Calendar.HOUR_OF_DAY),
					currentTime.get(Calendar.MINUTE), currentTime.get(Calendar.SECOND));
			//現在日時をLocalDateTimeへ変換
			LocalDateTime localCreatedDate = LocalDateTime.of(createdDate.get(Calendar.YEAR),
					createdDate.get(Calendar.MONTH),
					createdDate.get(Calendar.DAY_OF_MONTH), createdDate.get(Calendar.HOUR_OF_DAY),
					createdDate.get(Calendar.MINUTE), createdDate.get(Calendar.SECOND));
			//日時比較
			Duration defferenceDate = Duration.between(localCreatedDate, localCurrentTime);
			//日の差が1以上なら
			if (defferenceDate.toDays() >= 1) {
				contentData.get(i).setDefference_date(defferenceDate.toDays() + "日前");
				//時間の差が1以上なら
			} else if (defferenceDate.toHours() >= 1) {
				contentData.get(i).setDefference_date(defferenceDate.toHours() + "時間前");
				//分の差が1以上なら
			} else if (defferenceDate.toMinutes() >= 1) {
				contentData.get(i).setDefference_date(defferenceDate.toMinutes() + "分前");
			} else {
				//秒の差をセット
				contentData.get(i).setDefference_date(defferenceDate.getSeconds() + "秒前");
			}
		}
		// 返信を全件取得
		List<Comment> commentData = commentService.findAllComment();
		// ユーザーを全件取得
		List<User> userData = userService.findAllUser();
		//既読情報を取得
		@SuppressWarnings("unchecked")
		List<User> user = (List<User>) session.getAttribute("loginUser");
		Integer readUserId = user.get(0).getId();
		List<Read> readData = messageService.findReadByUserId(readUserId);
		for (int i = 0; i < contentData.size(); i++) {
			for (int j = 0; j < readData.size(); j++) {
				if (contentData.get(i).getId() == readData.get(j).getMessage_id()) {
					contentData.get(i).setAlready_read(1);
				}
			}
		}
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// 返信データオブジェクトを保管
		mav.addObject("comments", commentData);
		// ユーザーデータオブジェクトを保管
		mav.addObject("user", userData);
		//結合データオブジェクトを保管
		mav.addObject("messageData", messageData);
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		// 日付start
		mav.addObject("start", start);
		// 日付end
		mav.addObject("end", end);
		// 検索ワード
		mav.addObject("category", category);
		session.removeAttribute("errorMessages");

		if (start != null || end != null || category != null) {
			Page<PageMessage> pageMessages = pageService.searchMessageDateCategory(pageable, start, end, category);
	        model.addAttribute("page", pageMessages);
	        model.addAttribute("words", pageMessages.getContent());
	        model.addAttribute("url", "/ngoforum");
		} else {
			Page<PageMessage> pageMessages = pageService.getAllWord(pageable);
	        model.addAttribute("page", pageMessages);
	        model.addAttribute("words", pageMessages.getContent());
	        model.addAttribute("url", "/ngoforum");
		}


		return mav;
	}

	//絞り込み機能 日付
	@PostMapping("/Search")
	public ModelAndView date(@RequestParam(name = "start") String start,
			@RequestParam(name = "end") String end,
			@RequestParam(name = "category") String category,
			Model model, Pageable pageable,
			@RequestParam(defaultValue = "0") int page) {
		ModelAndView mav = new ModelAndView();
		page = page*10;
		// 絞り込み後の投稿の取得
		List<Message> contentData = messageService.searchMessageDateCategory(start, end, page ,category);
		// 返信を全件取得
		List<Comment> commentData = commentService.findAllComment();
		// ユーザーを全件取得
		List<User> userData = userService.findAllUser();

		//投稿日時の表示設定
		for (int i = 0; i < contentData.size(); i++) {
			contentData.get(i).getCreated_date();
			//投稿日時取得
			Calendar createdDate = Calendar.getInstance();
			createdDate.setTime(contentData.get(i).getCreated_date());
			//現在日時取得
			Date date = new Date();
			Calendar currentTime = Calendar.getInstance();
			currentTime.setTime(date);

			//投稿日時をLocalDateTimeへ変換
			LocalDateTime localCurrentTime = LocalDateTime.of(currentTime.get(Calendar.YEAR),
					currentTime.get(Calendar.MONTH),
					currentTime.get(Calendar.DAY_OF_MONTH), currentTime.get(Calendar.HOUR_OF_DAY),
					currentTime.get(Calendar.MINUTE), currentTime.get(Calendar.SECOND));
			//現在日時をLocalDateTimeへ変換
			LocalDateTime localCreatedDate = LocalDateTime.of(createdDate.get(Calendar.YEAR),
					createdDate.get(Calendar.MONTH),
					createdDate.get(Calendar.DAY_OF_MONTH), createdDate.get(Calendar.HOUR_OF_DAY),
					createdDate.get(Calendar.MINUTE), createdDate.get(Calendar.SECOND));
			//日時比較
			Duration defferenceDate = Duration.between(localCreatedDate, localCurrentTime);
			//日の差が1以上なら
			if (defferenceDate.toDays() >= 1) {
				contentData.get(i).setDefference_date(defferenceDate.toDays() + "日前");
				//時間の差が1以上なら
			} else if (defferenceDate.toHours() >= 1) {
				contentData.get(i).setDefference_date(defferenceDate.toHours() + "時間前");
				//分の差が1以上なら
			} else if (defferenceDate.toMinutes() >= 1) {
				contentData.get(i).setDefference_date(defferenceDate.toMinutes() + "分前");
			} else {
				//秒の差をセット
				contentData.get(i).setDefference_date(defferenceDate.getSeconds() + "秒前");
			}
		}

		//既読情報を取得
		@SuppressWarnings("unchecked")
		List<User> user = (List<User>) session.getAttribute("loginUser");
		Integer readUserId = user.get(0).getId();
		List<Read> readData = messageService.findReadByUserId(readUserId);
		for (int i = 0; i < contentData.size(); i++) {
			for (int j = 0; j < readData.size(); j++) {
				if (contentData.get(i).getId() == readData.get(j).getMessage_id()) {
					contentData.get(i).setAlready_read(1);
				}
			}
		}

		Page<PageMessage> pageMessages = pageService.searchMessageDateCategory(pageable, start, end, category);
        model.addAttribute("page", pageMessages);
        model.addAttribute("words", pageMessages.getContent());
        model.addAttribute("url", "/ngoforum");


		Date today = new Date();
		// Date型の日時をCalendar型に変換
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(today);
		// 日時を加算する
		calendar.add(Calendar.DATE, 1);
		// Calendar型の日時をDate型に戻す
		Date d1 = calendar.getTime();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// 返信データオブジェクトを保管
		mav.addObject("comments", commentData);
		// ユーザーデータオブジェクトを保管
		mav.addObject("user", userData);
		mav.addObject("day", d1);
		// 日付start
		mav.addObject("start", start);
		// 日付end
		mav.addObject("end", end);
		// 検索ワード
		mav.addObject("category", category);
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		session.setAttribute("start", start);
		session.setAttribute("end", end);
		session.setAttribute("category", category);
		return mav;
	}

	// 投稿内容表示画面
	@PostMapping("/AlreadyRead")
	public ModelAndView alreadyRead(@ModelAttribute("formModel") Read read) {
		Read readData = new Read();
		readData.setMessage_id(read.getMessage_id());
		readData.setUser_id(read.getUser_id());
		readData.setAreadey_read(1);
		Date date = new Date();
		readData.setCreated_date(date);
		readData.setUpdated_date(date);
		messageService.saveRead(readData);
		return new ModelAndView("redirect:/ngoforum");

	}
}
