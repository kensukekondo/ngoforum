package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Comment;
import com.example.demo.entity.ExtendedComment;
import com.example.demo.entity.ExtendedMessage;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.SummaryService;

@Controller
public class SummaryController {

	@Autowired
	SummaryService summaryService;

	@Autowired
	MessageService messageService;

	@Autowired
	CommentService commentService;

	@Autowired
	HttpSession session;

	// ユーザー一覧画面表示
	@GetMapping("/Summary")
	public ModelAndView summaryContent() {
		ModelAndView mav = new ModelAndView();

		// 各entityからデータを全件取得
		List<User> userData = summaryService.findAllUser();
		List<Message> messageData = summaryService.findAllMessage();
		List<Comment> commentData = summaryService.findAllComment();
		List<Branch> branchData = summaryService.findAllBranche();
		// 結合データー取得
		List<ExtendedMessage> extendedMessageData = messageService.findDate();
		List<ExtendedComment> extendedCommentData = commentService.findDate();
		int count;

		// ユーザーごとのメッセージ数
		for (int i = 0; i < userData.size(); i++) {
			count = 0;
			Integer user_id = userData.get(i).getId();
			for (int j = 0; j < messageData.size(); j++) {
				if(messageData.get(j).getUser_id() == user_id) {
					count = count + 1;
				}
			}
			userData.get(i).setMessageCount(count);
		}

		// ユーザーごとのコメント数
		for (int i = 0; i < userData.size(); i++) {
			count = 0;
			Integer user_id = userData.get(i).getId();
			for (int j = 0; j < commentData.size(); j++) {
				if(commentData.get(j).getUser_id() == user_id) {
					count = count + 1;
				}
			}
			userData.get(i).setCommentCount(count);
		}

		for (int i = 0; i < branchData.size(); i++) {
			count = 0;
			for (int j = 0; j < extendedMessageData.size(); j++) {
				if (extendedMessageData.get(j).getBranch_id() == i) {
					count = count + 1;
				}
			}
			branchData.get(i).setBranchMessageCount(count);
		}

		for (int i = 0; i < branchData.size(); i++) {
			count = 0;
			for (int j = 0; j < extendedCommentData.size(); j++) {
				if (extendedCommentData.get(j).getBranch_id() == i) {
					count = count + 1;
				}
			}
			branchData.get(i).setBranchCommentCount(count);
		}

		// ユーザーデータをセット
		mav.addObject("users", userData);
		// 支社データをセット
		mav.addObject("branches", branchData);
		// 画面遷移先を指定
		mav.setViewName("/Summary");
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		return mav;
	}
}
