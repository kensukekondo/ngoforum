package com.example.demo.controller;


import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.ExtendedMessage;
import com.example.demo.entity.Message;
import com.example.demo.entity.PageMessage;
import com.example.demo.entity.Read;
import com.example.demo.entity.User;
import com.example.demo.service.CommentService;
import com.example.demo.service.EditUserService;
import com.example.demo.service.LoginService;
import com.example.demo.service.MessageService;
import com.example.demo.service.PageService;
import com.example.demo.service.UserService;





@Controller
public class Login {
	@Autowired
	LoginService loginService;

	@Autowired
	CommentService commentService;

	@Autowired
	UserService userService;

	@Autowired
	MessageService messageService;

	@Autowired
	EditUserService edituserService;

	@Autowired
	PageService pageService;

	@Autowired
	HttpSession session;

	//ログイン画面表示
	@GetMapping("/Login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView();
		//画面推移先指定
		mav.setViewName("/Login");
		mav.addObject("errorMessagesLogin", session.getAttribute("errorMessagesLogin"));
		return mav;
	}

	//ログイン機能
	@PostMapping("/login")
	public ModelAndView loginContent(HttpServletRequest request, HttpServletResponse response, Model model, Pageable pageable) {

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		if((account == "") && (password == "")){
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("アカウントが入力されていません");
			errorMessages.add("パスワードが入力されていません");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.setViewName("/Login");
			return mav;
		}else if(account == "") {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("アカウントが入力されていません");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.setViewName("/Login");
			return mav;
		}else if(password == "") {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("パスワードが入力されていません");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.addObject("formModel",account);
			mav.setViewName("/Login");
			return mav;
		}



		List<User> accountDate = loginService.selectAccount(account,password);

		if(accountDate.size() == 0) {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("アカウントまたはパスワードが誤っています");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.addObject("formModel",account);
			mav.setViewName("/Login");
			return mav;
		}
		if ((accountDate.size() == 1) && (accountDate.get(0).getIs_stopped() == 1)){
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("アカウントまたはパスワードが誤っています");
			mav.addObject("errorMessagesLogin", errorMessages);
			mav.addObject("formModel",account);
			mav.setViewName("/Login");
			return mav;
		}

		if (session.getAttribute("loginUser") != null) {
			ModelAndView mav = new ModelAndView();
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("既にログイン済です");
			// 投稿を絞り込み取得
			List<Message> contentData = messageService.findLimitMessage(0);
			//結合データー取得
			List<ExtendedMessage> messageData = messageService.findDate();
			for(int i = 0; i < contentData.size(); i ++) {
				contentData.get(i).setBranch_id(messageData.get(i).getBranch_id());
				contentData.get(i).setDepartment_id(messageData.get(i).getDepartment_id());
			}
			//投稿日時の表示設定
			for (int i = 0; i < contentData.size(); i++) {
				contentData.get(i).getCreated_date();
				//投稿日時取得
				Calendar createdDate = Calendar.getInstance();
				createdDate.setTime(contentData.get(i).getCreated_date());
				//現在日時取得
				Date date = new Date();
				Calendar currentTime = Calendar.getInstance();
				currentTime.setTime(date);

				//投稿日時をLocalDateTimeへ変換
				LocalDateTime localCurrentTime = LocalDateTime.of(currentTime.get(Calendar.YEAR),
						currentTime.get(Calendar.MONTH),
						currentTime.get(Calendar.DAY_OF_MONTH), currentTime.get(Calendar.HOUR_OF_DAY),
						currentTime.get(Calendar.MINUTE), currentTime.get(Calendar.SECOND));
				//現在日時をLocalDateTimeへ変換
				LocalDateTime localCreatedDate = LocalDateTime.of(createdDate.get(Calendar.YEAR),
						createdDate.get(Calendar.MONTH),
						createdDate.get(Calendar.DAY_OF_MONTH), createdDate.get(Calendar.HOUR_OF_DAY),
						createdDate.get(Calendar.MINUTE), createdDate.get(Calendar.SECOND));
				//日時比較
				Duration defferenceDate = Duration.between(localCreatedDate, localCurrentTime);
				//日の差が1以上なら
				if (defferenceDate.toDays() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toDays() + "日前");
					//時間の差が1以上なら
				} else if (defferenceDate.toHours() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toHours() + "時間前");
					//分の差が1以上なら
				} else if (defferenceDate.toMinutes() >= 1) {
					contentData.get(i).setDefference_date(defferenceDate.toMinutes() + "分前");
				} else {
					//秒の差をセット
					contentData.get(i).setDefference_date(defferenceDate.getSeconds() + "秒前");
				}
			}
			// 返信を全件取得
			List<Comment> commentData = commentService.findAllComment();
			// ユーザーを全件取得
			List<User> userData = userService.findAllUser();
			//既読情報を取得
			@SuppressWarnings("unchecked")
			List<User> user = (List<User>) session.getAttribute("loginUser");
			Integer readUserId = user.get(0).getId();
			List<Read> readData = messageService.findReadByUserId(readUserId);
			for (int i = 0; i < contentData.size(); i++) {
				for (int j = 0; j < readData.size(); j++) {
					if (contentData.get(i).getId() == readData.get(j).getMessage_id()) {
						contentData.get(i).setAlready_read(1);
					}
				}
			}
			// 画面遷移先を指定
			mav.setViewName("/top");
			// 投稿データオブジェクトを保管
			mav.addObject("contents", contentData);
			// 返信データオブジェクトを保管
			mav.addObject("comments", commentData);
			// ユーザーデータオブジェクトを保管
			mav.addObject("user", userData);
			//結合データオブジェクトを保管
			mav.addObject("messageData", messageData);
			mav.addObject("loginUser", session.getAttribute("loginUser"));

			Page<PageMessage> pageMessages = pageService.getAllWord(pageable);
	        model.addAttribute("page", pageMessages);
	        model.addAttribute("words", pageMessages.getContent());
	        model.addAttribute("url", "/ngoforum");
			mav.addObject("errorMessages", errorMessages);
			return mav;
		}

		ModelAndView mav = new ModelAndView();
		mav.addObject("loginUser",accountDate);

		User user = loginService.editUser(accountDate.get(0).getId());

		Date today = new Date();

		// ログイン時間をセット
		user.setLogin_date(today);

		// ログイン時間を更新
		edituserService.updateUser(user);

		//TOP画面に推移
		mav.setViewName("redirect:/ngoforum");
		session.setAttribute("loginUser", accountDate);
		return mav;
	}

}
