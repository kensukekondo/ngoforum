package com.example.demo.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.EditUserService;

@Controller
public class EditUserController {

	@Autowired
	EditUserService edituserService;

	@Autowired
	HttpSession session;

	// ユーザー編集画面表示
	@GetMapping("/EditUser/{id}")
	public ModelAndView editContent(@PathVariable String id) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		if(!id.matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("forward:/ManageUser");
			return mav;
		}

		int userId = Integer.parseInt(id);
		List<User> userDate = edituserService.selectUser(userId);

		// 編集するアカウントを取得
		User user = edituserService.editUser(userId);

		if(userDate.size() == 0) {
			errorMessages.add("不正なパラメータが入力されました");
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("forward:/ManageUser");
			return mav;
		}

		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String updated_date = df.format(user.getUpdated_date());

		// 編集するアカウントをセット
		mav.addObject("formModel", user);
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		mav.addObject("updatedDate", updated_date);
		// 画面遷移先を指定
		mav.setViewName("/EditUser");
		return mav;
	}

	// idがnullの場合エラーを表示
	@GetMapping("/EditUser/")
	public ModelAndView nullContent() {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメータが入力されました");
		mav.addObject("errorMessages", errorMessages);
		mav.setViewName("forward:/ManageUser");
		return mav;
	}

	// ユーザー編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent (@PathVariable Integer id,
			@ModelAttribute("formModel") User user,
			@RequestParam(name = "password") String password,
			@RequestParam(name = "passwordCheck") String passwordCheck,
			@RequestParam(name = "account") String account,
			@RequestParam(name = "updated_date") String updated_date) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		// アカウントを取得
		User userAccountData = edituserService.editUser(id);

		// アカウント名の取得
		String userAccount = userAccountData.getAccount();

		// パスワードの入力がない場合保持
		if (password == "") {
		user.setPassword(userAccountData.getPassword());
		}

		// データベースの更新時間を取得
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String datebaseUpdated_date = df.format(userAccountData.getUpdated_date());

		// アカウントを全件取得
		List<User> confirmAccount = edituserService.selectAccount(account);

		// UrlParameterのidを更新するentityにセット
		user.setId(id);

		// アカウントが空白の場合
		if (user.getAccount() == "") {
			errorMessages.add("アカウントを入力してください");
		// アカウント名が6文字満たない場合
		} else if (!(user.getAccount().length() >= 6)){
			errorMessages.add("アカウントは6文字以上で入力してください");
		// アカウント名が重複している場合
		} else if (confirmAccount.size() == 1 && !(userAccount.equals(account))) {
			errorMessages.add("アカウントが重複しています");
		// アカウント名が20文字を超えているの場合
		} else if(!(user.getAccount().length() <= 20)) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		}

		// パスワードが6文字満たないの場合
		if (!(user.getPassword().length() >= 6) && !(password == "")) {
			errorMessages.add("パスワードは6文字以上で入力してください");
		}

		//パスワードが20文字超えている場合
		if (!(user.getPassword().length() <= 20)) {
			errorMessages.add("パスワードは20文字以下で入力してください");
		}

		// パスワードと確認パスワードが同一でないの場合
		if (!(user.getPassword().equals(passwordCheck)) && !(password == "")) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		// ユーザー名が空白の場合
		if (user.getName() == "") {
			errorMessages.add("ユーザー名を入力してください");

		//ユーザー名が10文字超えている場合
		} else if (!(user.getName().length() <= 10)) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}

		// 支社が未選択の場合
		if (user.getBranch_id() == null) {
			errorMessages.add("支社を選択してください");
		}

		// 部署が未選択の場合
		if (user.getDepartment_id() == null) {
			errorMessages.add("部署を選択してください");
		}

		// 支社と部署の組み合わせが不正の場合
		if (user.getBranch_id() == 0) {
			if(user.getDepartment_id() == 2 || user.getDepartment_id() == 3) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}
		if (user.getBranch_id() == 1 || user.getBranch_id() == 2 || user.getBranch_id() == 3) {
			if(user.getDepartment_id() == 0 || user.getDepartment_id() == 1) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		// 更新が重複した場合
		if (!(datebaseUpdated_date.equals(updated_date))) {
			errorMessages.add("更新が重複しています");
		}

		if (errorMessages.size() != 0) {
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("loginUser", session.getAttribute("loginUser"));
			mav.addObject("updatedDate", updated_date);
			mav.setViewName("/EditUser");
			return mav;
		}

		// 更新時間をセット
		Date currentDate = new Date();
		user.setUpdated_date(currentDate);

		// 作成時間をセット
		user.setCreated_date(userAccountData.getCreated_date());

		// ログイン時間をセット
		user.setLogin_date(userAccountData.getLogin_date());

		user.setIs_stopped(0);

		// 編集した投稿を更新
		edituserService.updateUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/ManageUser");
	}

}
