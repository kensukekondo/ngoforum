package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.SignUpService;

@Controller
public class SignUpController {

	@Autowired
	SignUpService signupService;


	//新規登録ボタン表示
	@GetMapping("/SignUp")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		User user = new User();
		// 画面遷移先を指定
		mav.setViewName("/SignUp");
		// 準備した空のentityを保管
		mav.addObject("formModel", user);
		return mav;
	}

	// アカウント登録処理
	@PostMapping("/SignUp")
	public ModelAndView addUser(@ModelAttribute("formModel") User user,
			@RequestParam(name = "passwordCheck") String passwordCheck,
			@RequestParam(name = "account") String account) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();

		// アカウントを全件取得
		List<User> confirmAccount = signupService.selectAccount(account);

		Date date = new Date();
		user.setCreated_date(date);
		user.setUpdated_date(date);
		user.setLogin_date(date);
		user.setIs_stopped(0);

		// アカウントが空白の場合
		if (user.getAccount() == "") {
			errorMessages.add("アカウント名を入力してください");
			//アカウント名が6文字満たない場合
		} else if (!(user.getAccount().length() >= 6)){
			errorMessages.add("アカウントは6文字以上で入力してください");
			//アカウントが重複している場合
		} else if (confirmAccount.size() == 1) {
			errorMessages.add("アカウントが重複しています");
			//アカウント名が20文字を超えているの場合
		} else if(!(user.getAccount().length() <= 20)) {
			errorMessages.add("アカウントは20文字以下で入力してください");
		}

		// パスワードが空白の場合
		if (user.getPassword() == "") {
			errorMessages.add("パスワードを入力してください");
		// パスワードが6文字満たないの場合
		} else if (!(user.getPassword().length() >= 6)) {
			errorMessages.add("パスワードは6文字以上で入力してください");
		}

		//パスワードが20文字超えている場合
		if (!(user.getPassword().length() <= 20)) {
			errorMessages.add("パスワードは20文字以下で入力してください");
		}

		// パスワードと確認パスワードが同一でないの場合
		if (!(user.getPassword().equals(passwordCheck))) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
		}

		//ユーザー名が空白の場合
		if(user.getName() == "") {
			errorMessages.add("ユーザー名を入力してください");
			//ユーザー名が10文字超えている場合
		} else if (!(user.getName().length() <= 10)) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}

		// 支社が未選択の場合
		if (user.getBranch_id() == null) {
			errorMessages.add("支社を選択してください");
		}

		// 部署が未選択の場合
		if (user.getDepartment_id() == null) {
			errorMessages.add("部署を選択してください");
		}

		// 支社と部署の組み合わせが不正の場合
		if (user.getBranch_id() == 0) {
			if(user.getDepartment_id() == 2 || user.getDepartment_id() == 3) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}
		if (user.getBranch_id() == 1 || user.getBranch_id() == 2 || user.getBranch_id() == 3) {
			if(user.getDepartment_id() == 0 || user.getDepartment_id() == 1) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		if (errorMessages.size() != 0) {
			User userValue = new User(); // form用の空のentityを準備
			userValue.setAccount(user.getAccount());
			userValue.setName(user.getName());
			userValue.setBranch_id(user.getBranch_id());
			userValue.setDepartment_id(user.getDepartment_id());
			mav.addObject("formModel", userValue); // 値を保持
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/SignUp");
			return mav;
		}

		// 投稿をテーブルに格納
		signupService.saveUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/ManageUser");
	}

}