package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.ManageUserService;
import com.example.demo.service.UserService;

@Controller
public class ManageUserController {

	@Autowired
	ManageUserService manageuserService;
	@Autowired
	UserService userService;
	@Autowired
	HttpSession session;

	// ユーザー一覧画面表示
	@GetMapping("/ManageUser")
	public ModelAndView editContent() {
		ModelAndView mav = new ModelAndView();
		// 返信を全件取得
		List<User> UserData = manageuserService.findAllUser();
		// 編集する投稿をセット
		mav.addObject("users", UserData);
		// 画面遷移先を指定
		mav.setViewName("/ManageUser");
		mav.addObject("loginUser", session.getAttribute("loginUser"));
		return mav;
	}

	// ユーザー復活停止処理
	@PutMapping("/changeStatus/{id}")
	public ModelAndView changeStatusContent (@PathVariable Integer id, @ModelAttribute("formModel") User user,
			@RequestParam(name = "is_stopped") short is_stopped) {

		// UrlParameterのidを更新するentityにセット
		user.setId(id);

		User userDate = userService.editUser(id);
		user.setCreated_date(userDate.getCreated_date());
		user.setUpdated_date(userDate.getUpdated_date());
		user.setLogin_date(userDate.getLogin_date());

		if (is_stopped == 0) {
			user.setIs_stopped(1);
		} else {
			user.setIs_stopped(0);
		}

		// 編集した投稿を更新
		manageuserService.changeStatusUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/ManageUser");
	}
}
