package com.example.demo.filter;

import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Bean
	public FilterRegistrationBean<LoginFilter> loggingFilter2() {
		FilterRegistrationBean<LoginFilter> registrationBean = new FilterRegistrationBean<>();

		registrationBean.setFilter(new LoginFilter());
		registrationBean.addUrlPatterns("/EditUser/*");
		registrationBean.addUrlPatterns("/NewMessage/*");
		registrationBean.addUrlPatterns("/Summary");
		registrationBean.addUrlPatterns("/ngoforum");

		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<ManageFilter> loggingFilter1() {
		FilterRegistrationBean<ManageFilter> registrationBean = new FilterRegistrationBean<>();

		registrationBean.setFilter(new ManageFilter());
		registrationBean.addUrlPatterns("/EditUser/*");
		registrationBean.addUrlPatterns("/SignUp");
		registrationBean.addUrlPatterns("/Summary");
		registrationBean.addUrlPatterns("/ManageUser");

		return registrationBean;
	}

	@Override
	  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
	      PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
	      //ページ単位に表示する件数
	      resolver.setFallbackPageable(PageRequest.of(0, 10, Sort.unsorted()));
	      argumentResolvers.add(resolver);
	      super.addArgumentResolvers(argumentResolvers);
	  }

}
