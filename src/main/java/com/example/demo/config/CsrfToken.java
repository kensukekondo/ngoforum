package com.example.demo.config;

import java.io.Serializable;

public interface CsrfToken extends Serializable {
	String getHeaderName();

	String getParameterName();

	String getToken();
}