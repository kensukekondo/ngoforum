package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;

@Repository
public interface SignUpRepository extends JpaRepository<User, Integer> {

	@Query("select t from User t where t.account = :account")
	List<User>findByAccount(String account);
}