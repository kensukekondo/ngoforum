package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.User;

@Repository
public interface ManageUserRepository extends JpaRepository<User, Integer> {
	@Query("select users from User users ORDER BY users.created_date DESC")
	List<User> findAll();

}