package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Read;

	@Repository
	public interface ReadRepository extends JpaRepository<Read, Integer> {
		@Query("select read from Read read where read.user_id = :readUserId")
		List<Read> findReadByUserId(Integer readUserId);

	}

