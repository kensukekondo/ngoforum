package com.example.demo.repository;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Comment;
import com.example.demo.entity.ExtendedComment;

	@Repository
	public interface CommentRepository extends JpaRepository<Comment, Integer> {

		@Query("select comments from Comment comments ORDER BY comments.created_date DESC")
		List<Comment> findAll();

		@Query(value = "select u.account,u.name,u.branch_id,u.department_id,c.user_id,c.text,c.message_id,c.created_date,c.updated_date "
				+"from users u "
				+ "inner join comments c on u.id = c.user_id "
				+ "order by c.created_date desc ",
				nativeQuery = true)
		List<Object[]> findByCommentDate();

		default List<ExtendedComment> findCommentDate() {
	        return findByCommentDate()
	            .stream()
	            .map(ExtendedComment::new)
	            .collect(Collectors.toList());
	    }
}
