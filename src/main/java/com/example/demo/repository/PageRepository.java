package com.example.demo.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.PageMessage;

@Repository
public interface PageRepository extends CrudRepository<PageMessage, Long>{

    public Page<PageMessage> findAll(Pageable pageable);

    @Query(nativeQuery = true , value = "select * from messages where created_date between :startDate and :endDate "
			+ "ORDER BY created_date DESC ")
    public Page<PageMessage> findByLimitDateBetween(Pageable pageable, Date startDate, Date endDate);

    //カテゴリーのみ絞り込み
    @Query(nativeQuery = true , value = "select * from messages  where category like %:search% ESCAPE '~' ORDER BY created_date DESC ")
    public Page<PageMessage> findAllByLikeCategory(Pageable pageable,@Param("search") String search);

    //日付とカテゴリーで絞り込み
    @Query(nativeQuery = true, value = "select * from messages where category like %:category% ESCAPE '~' and created_date between :startDate and :endDate ORDER BY created_date DESC ")
    public Page<PageMessage> findByDateCategory(Pageable pageable, Date startDate, Date endDate, @Param("category") String category);
}