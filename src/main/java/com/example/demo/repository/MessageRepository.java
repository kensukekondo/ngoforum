package com.example.demo.repository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.ExtendedMessage;
import com.example.demo.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
	@Query(nativeQuery = true , value = "select * from messages where created_date between :startDate and :endDate "
			+ "ORDER BY created_date DESC LIMIT 10 OFFSET :page")
	List<Message> findByLimitDateBetween(Date startDate, Date endDate,int page);

	@Query(nativeQuery = true , value = "SELECT * FROM messages ORDER BY created_date DESC")
	List<Message> findAll();

	@Query(nativeQuery = true , value = "SELECT * FROM messages ORDER BY created_date DESC LIMIT 10 OFFSET :page")
	List<Message> findLimit(int page);

	//カテゴリーのみ絞り込み
	@Query(nativeQuery = true , value = "select * from messages  where category like %:search% ESCAPE '~' ORDER BY created_date DESC LIMIT 10 OFFSET :page")
	List<Message> findAllByLikeCategory(int page,@Param("search") String search);

	//日付とカテゴリーで絞り込み
	@Query(nativeQuery = true, value = "select * from messages where category like %:category% ESCAPE '~' and created_date between :startDate and :endDate ORDER BY created_date DESC LIMIT 10 OFFSET :page")
	List<Message> findByDateCategory(Date startDate, Date endDate, int page,@Param("category") String category);

	@Query(value = "select u.account,u.name,u.branch_id,u.department_id,m.user_id,m.title,m.text,m.category,m.created_date,m.updated_date "
			+"from users u "
			+ "inner join messages m on u.id = m.user_id "
			+ "order by m.created_date desc ",
			nativeQuery = true)
	List<Object[]> findByMessageDate();

	default List<ExtendedMessage> findMessageDate() {
        return findByMessageDate()
            .stream()
            .map(ExtendedMessage::new)
            .collect(Collectors.toList());
    }

}