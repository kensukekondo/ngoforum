package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NgoforumApplication {

	public static void main(String[] args) {
		SpringApplication.run(NgoforumApplication.class, args);
	}

}
